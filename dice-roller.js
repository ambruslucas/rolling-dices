
function getRandom(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function  gerarValores() {    
    let count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];                        /* cria loop para a função getRandom */
    for (let i = 1; i <= 1000; i++) {
        let dado1 = getRandom(1, 6);
        let dado2 = getRandom(1, 6);
        
        let soma = dado1 + dado2;

        count[soma] = count[soma] + 1;
    }

    document.getElementById("barra1").style.width = `${(count[2]) * 2}px`;
    document.getElementById("barra2").style.width = `${(count[3]) * 2}px`;
    document.getElementById("barra3").style.width = `${(count[4]) * 2}px`;
    document.getElementById("barra4").style.width = `${(count[5]) * 2}px`;
    document.getElementById("barra5").style.width = `${(count[6]) * 2}px`;
    document.getElementById("barra6").style.width = `${(count[7]) * 2}px`;
    document.getElementById("barra7").style.width = `${(count[8]) * 2}px`;
    document.getElementById("barra8").style.width = `${(count[9]) * 2}px`;
    document.getElementById("barra9").style.width = `${(count[10]) * 2}px`;
    document.getElementById("barra10").style.width = `${(count[11]) * 2}px`;
    document.getElementById("barra11").style.width = `${(count[12]) * 2}px`;

    document.getElementById("barra1").innerHTML = count[2];
    document.getElementById("barra2").innerHTML = count[3];
    document.getElementById("barra3").innerHTML = count[4];
    document.getElementById("barra4").innerHTML = count[5];
    document.getElementById("barra5").innerHTML = count[6];
    document.getElementById("barra6").innerHTML = count[7];
    document.getElementById("barra7").innerHTML = count[8];
    document.getElementById("barra8").innerHTML = count[9];
    document.getElementById("barra9").innerHTML = count[10];
    document.getElementById("barra10").innerHTML = count[11];
    document.getElementById("barra11").innerHTML = count[12];

    console.log(count);
}

// transformar id do html barra1 em js  

// barra1.style.width = "50px" 

// barraum.style.background = "blue";

//conjunto.length       => tamanho do array
//conjunto[i]    => valor do array
//contador              => indice do array
